
'use strict';

const Colors = require('colors');
const Gulp = Reflect.get(global, 'gulp@abstractor');
const GulpConnect = require('gulp-connect');
const WebSocketServer = require('ws').Server;
const Net = require('net');


class LocalServer {
  constructor(settings, build) {
    this.settings = settings;
    this.build = build;
    this.appName = settings.name;
    this.name = `local-server-${settings.name}`;
    this.plugins = settings.plugins;
    this.defaultSite = settings.defaultSite;
    this.parent = settings.parent;
    this.httpServers = [];
    this.httpsServers = [];
    this.wsServers = [];
    this.appActor = null;
    Gulp.task(this.name, this.run.bind(this));
  }
  
  run(cb) {
    if(this.build) {
      return cb();
    }
    if(this.settings.httpServers) {
      this.settings.httpServers.forEach((httpServer) => {
        this.addHttpServer(httpServer.name, httpServer.parameters.host.value, httpServer.parameters.port.value, httpServer.parameters.directories);
      });
    }
    if(this.settings.httpsServers) {
      this.settings.httpsServers.forEach((httpsServer) => {
        this.addHttpsServer(httpsServer.name, httpsServer.parameters.host.value, httpsServer.parameters.port.value, httpsServer.parameters.directories, httpsServer.keyFile, httpsServer.certFile);
      });
    }
    if(this.settings.wsServers) {
      this.settings.wsServers.forEach((wsServer) => {
        this.addWsServer(wsServer.name, wsServer.parameters.host.value, wsServer.parameters.port.value);
      });
    }
    this.appActor = require('z-abs-corelayer-server/app-actor');
    this.appActor.initMainServer(this.defaultSite, this.plugins, this.wsServers, () => {
      let pendings = 0;
      this.wsServers.forEach((wsServer) => {
        ++pendings;
        this._createWsServer(wsServer, () => {
          if(0 === --pendings) {
            this._started(cb);
          }
        });
      });
      this.httpServers.forEach((httpServer) => {
        ++pendings;
        this._createHttpServer(httpServer, () => {
          if(0 === --pendings) {
            this._started(cb);
          }
        });
      });
      this.httpsServers.forEach((httpsServer) => {
        ++pendings;
        this._createHttpsServer(httpsServer, (err) => {
          if(0 === --pendings) {
            this._started(cb);
          }
        });
      });
    });
  }
  
  addHttpServer(name, host, port, paths) {
    this.httpServers.push({
      name: name,
      host: host,
      port: port,
      paths: paths,
      server: null
    });
  }
  
  addHttpsServer(name, host, port, paths, keyFile, certFile) {
    this.httpsServers.push({
      name: name,
      host: host,
      port: port,
      paths: paths,
      keyFile: keyFile,
      certFile: certFile,
      server: null
    });
  }
  
  addWsServer(name, host, port) {
    this.wsServers.push({
      name: name,
      host: host,
      port: port,
      server: null
    });
  }
  
  _createHttpServer(httpServer, cb) {
    let handle = null;
    if(`${this.appName}-http-server` === httpServer.name) {
      handle = this.appActor.handleHttpRequest.bind(this.appActor);
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
    httpServer.server = GulpConnect.server({
      root: httpServer.paths,
      host: httpServer.host,
      port: httpServer.port,
      name: httpServer.name,
      livereload: false,
      middleware: (connect, opt) => {
        return [(req, res, next) => {
          handle(req, res, next);
        }];
      }
    }, cb);
    httpServer.server.server.on('error', (err) => {
      const date = new Date();
      console.log('[' + `${date.getHours()}`.padStart(2, '0').grey + ':' + `${date.getMinutes()}`.padStart(2, '0').grey + ':' + `${date.getSeconds()}`.padStart(2, '0').grey + ']', 'Could not start '.red + `'${this.appName}-http-server'`, 'http error: '.red, err);
    });
  }
  
  _createHttpsServer(httpsServer, cb) {
    let handle = null;
    if(`${this.appName}-https-server` === httpsServer.name) {
      handle = this.appActor.handleHttpRequest.bind(this.appActor);
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
    const f = () => {
      const Fs = require('fs');
      try {
        const keyFile = Fs.readFileSync(httpsServer.keyFile);
        const certFile = Fs.readFileSync(httpsServer.certFile);
        return {
          key: keyFile,
          cert: certFile
        };
      }
      catch(err) {
        console.log(`Could not get cert and key file, err`, err);
        return true;
      }
    };
    const https = 'localhost' === httpsServer.host ? true : f();
    httpsServer.server = GulpConnect.server({
      https: https,
      root: httpsServer.paths,
      host: httpsServer.host,
      port: httpsServer.port,
      name: httpsServer.name,
      livereload: false,
      middleware: (connect, opt) => {
        return [(req, res, next) => {
          handle(req, res, next);
        }];
      }
    }, cb);
    httpsServer.server.server.on('error', (e) => {
      const date = new Date();
      console.log('[' + `${date.getHours()}`.padStart(2, '0').grey + ':' + `${date.getMinutes()}`.padStart(2, '0').grey + ':' + `${date.getSeconds()}`.padStart(2, '0').grey + ']', 'Could not start '.red + `'${this.appName}-https-server'`, 'https error: '.red, err);
    });
  }
  
  _createWsServer(wsServer, cb) {
    let handle = null;
    if(`${this.appName}-ws-server` === wsServer.name) {
      handle = this.appActor.handleHttpRequest.bind(this.appActor);
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
    const wss = new WebSocketServer({
      perMessageDeflate: false,
      host: wsServer.host,
      port: wsServer.port
    }, () => {
      const date = new Date();
      console.log('[' + `${date.getHours()}`.padStart(2, '0').grey + ':' + `${date.getMinutes()}`.padStart(2, '0').grey + ':' + `${date.getSeconds()}`.padStart(2, '0').grey + ']', `${wsServer.name} started ws://${wsServer.host}:${wsServer.port}`.green);
      cb();  
    });
    wss.on('connection', (ws) => {
      this.appActor.handleWsConnection(ws);
    });
    wss.on('error', (err) => {
      const date = new Date();
      console.log('[' + `${date.getHours()}`.padStart(2, '0').grey + ':' + `${date.getMinutes()}`.padStart(2, '0').grey + ':' + `${date.getSeconds()}`.padStart(2, '0').grey + ']', 'Could not start '.red + `'${this.appName}-ws-server'`, 'ws error: '.red, err);
    });
  }
  
  _started(cb) {
    const host = this.parent.host.value;
    const port = this.parent.port.value;
    if(0 !== port) {
      const socket = Net.connect({
        port: port,
        host: host
      }, (err) => {
        if(!err) {
          socket.write(JSON.stringify({result: 'success', pid: process.pid}) + '\r\n', () => {
            console.log(`${this.name} started - Succeeded to connect to parent process(${host}:${port})`);
            cb();
          });
        }
        else {
          console.log(`${this.name} started - Failed to connect to parent process(${host}:${port})`);
          cb();
        }
      });
      socket.on('error', (err) => {
        console.log(`${this.name} started - Failed to connect to parent process`, err);
        cb();
      });
    }
    else {
      console.log(`${this.name} started - No parent process`);
      cb();
    }
  }
}


module.exports = LocalServer;
